<?php
/*----------------page adminstrateur----------------*/
require_once("./model/adminManager.php");
require_once("./model/articleManager.php");
require_once("./model/commentaireManager.php");


function formulaire(){
	require ("./view/frontend/inscription.php");
}

function signOut(){
	require('./index.php');
}

/*--------------------------------connexion----------------------------------------*/
function checkInfo($checkPseudo,$checkmdp){
	$checkUser= new membersManager();
	$userLogin= $checkUser->checkInfo($checkPseudo,$checkmdp);
	//Redrigier vers pour checker the Admin.php
}

function subscribe($lastname,$firstname,$pseudo,$mdp,$mail,$pseudoPresent){
	$newMember= new membersManager();
	$subMember= $newMeinfoImber->getNewUser($lastname,$firstname,$pseudo,$mdp,$mail,$pseudoPresent);
	//Redrigier vers pour s'inscrire the Admin.php
}

function adminConnexion($AdminPseudo,$AdminPwd){
	$adminlog= new membersManager();
	$infoAdmin= $adminlog->AdminCheckInfo($AdminPseudo,$AdminPwd);
	require("./view/backend/adminPage.php");
}



/*--------------------------------message connexion----------------------------------------*/
function msgPWD($message){
	$message;

	require('./view/frontend/inscription.php');
}
function msgMail($message){
	$message;

	require('./view/frontend/inscription.php');
}

function infoIssues($infoIssues){
	$infoIssues;
	require('./view/frontend/inscription.php');
}
function noNickName($noNickName){

	$noNickName;
	require('./view/frontend/inscription.php');
}
function NoMatch($NoMatch){
	$NoMatch;

	require('./view/frontend/inscription.php');
}
/*---------------------------------fin de message---------------------------------------*/

function updateWarningComm($warningComm,$article_id){
	$signalement= new commentaireManager();
	$warningComment=$signalement->signalComm($warningComm,$article_id);
	require('./view/pages/chapitre.php');
}

/*--------------------------------fin de connexion----------------------------------------*/

/*--------------------------------admistrateur----------------------------------------*/
function updateArticle(){
	$callArticle= new ArticleManager();
	$listArticlesToEdit=$callArticle-> listArticlesToEdit();

	$repotedCommentaire= new CommentaireManager();
	$reportCommentaire= $repotedCommentaire->reportCommentaire();

	require("./view/backend/adminPage.php");
}
/*--------------------------------Article----------------------------------------*/
function getArticle($title_article,$content_article){
	$getNewArticle=new articleManager();
	$newArticle= $gettNewArticle->getArticle($title_article,$content_article);
	//redrigier vers la page  Adminpage.php
}

function editArticle(){
	$callArticle= new ArticleManager();
	$pickOneArticle=$callArticle->getArticle();

	require("./view/frontend/inscription.php");
}

function TinymceArticle($id_edit,$title_edit,$text_edit){
	$editArticle= new  ArticleManager();
	$TinymceArticle=$editArticle->editArticle($id_edit,$title_edit,$text_edit));
	//Redrigier vers Adminpage.php tinymce
}

function deleteArticle($article_id){
	$deleteArticle= new ArticleManager();
	$deleteOneArticle= $deleteArticle->earseCommentaire($article_id);

	$deleteAllCommentaires= new CommentaireManager();
	$deleteArticle= $deletedAllCommentaires-> deleteAllCommentaires($article_id);
	//Redrigier vers pour la suppression Adminpage.php
}
/*--------------------------------FIN CHAPITRES----------------------------------------*/

/*--------------------------------DEBUT COMMENTAIRES----------------------------------------*/
function deleteCommentaire($id_commentaire){
	$eraseCommentaire= new CommentaireManager();
	$erase=$eraseCommentaire->deleteCommentaire($id_commentaire);
	//Redrigier vers pour la suppression commentaireManager.php
}

function reportCommentaire($id_commentaire){
	$getReportedCommentaires= new CommentaireManager();
	$commentOk= $getReportedCommentaires-> commentaireValidation($id_commentaire);
	//Redrigier vers  commentaireManager.php
}
/*--------------------------------FIN COMMENTAIRES----------------------------------------*/

/*--------------------------------FIN ADMINSTRATEUR----------------------------------------*/