<?php
require_once("./model/frontend/article.php");
require_once("./model/frontend/commentaire.php");

function  headerBanner(){
	require ("./view/frontend/header.php");
}
/*------------------Article-----------------------*/
function listArticles(){
	$callArticle= new ArticleManager();
	$listArticles=$callArticle-> listArticles();
	require("./view/frontend/article.php");
}

function getArticle(){
	$callArticle= new ArticleManager();
	$getArticle=$callArticle->getArticle();

	$listCommentaires= new CommentaireManager();
	$getArticle=$listCommentaires->getArticle(();

	require("./view/frontend/article.php");
}
/*------------------fin de article-----------------------*/

/*------------------commentaires-----------------------*/
function addCommentaire($commentaire_author,$commentaire_content,$articles_id){
	$addCommmentaire= new CommentaireManager();
	$reportCommentaire=$addCommentaire->addCommentaire($commentaire_author,$commentaire_content,$articles_id);

	$listCommentaires= new CommentaireManager();
	$getArticle=$listCommentaires->getArticle(();
	require('./view/frontend/article.php');
}