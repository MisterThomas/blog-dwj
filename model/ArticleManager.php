<?php
class ArticleManager {

    private $_db;

    public function __construct($db) {
        $this->setDb($db);
    }

    public function getArticle($article_ID) {
        $article = [];

        $q = $this->_db->prepare('SELECT article_id, article_title, article_content, article_author, DATE_FORMAT(article_modified_date, \'%d/%m/%Y à %Hh%imin%ss\') AS modified_date_fr, DATE_FORMAT(article_creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS date_fr FROM articles WHERE article_id = ?');
        $q->execute(array($article_ID));
        
        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $article = new Article($data);
        }
        return $article;
    }

    public function addArticle($article_title, $article_content, $article_author) {
        $q = $this->_db->prepare('INSERT INTO articles (article_title, article_content, article_author, article_creation_date) VALUES (?, ?, ?, NOW())');
        $articleToAdd = $q->execute(array($article_title, $article_content, $article_author));

        $addedArticle = [];
        $req = $this->_db->prepare('SELECT article_title, article_content, article_author, article_id, DATE_FORMAT(article_creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS date_fr FROM articles WHERE article_title = ?');
        $req->execute(array($article_title));

        while ($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $addedArticle = new Article($data);
        }
        return $addedArticle;
    }

    public function updateArticle($article_title, $article_content, $article_id) {
        $q = $this->_db->prepare('UPDATE articles SET article_title = ?, article_content = ?, article_modified_date = NOW() WHERE article_id = ?');
        $articleToUpdate = $q->execute(array($article_title, $article_content, $article_id));
        return $articleToUpdate;
    }

    public function deleteArticle($article_id) {
        $q = $this->_db->prepare('DELETE FROM articles WHERE article_id = ?');
        $articleToDelete = $q->execute(array($article_id));
        return $articleToDelete;
    }

    public function listArticles() {
        $articles = [];
        $q = $this->_db->query('SELECT article_id, article_title, article_content, article_author, DATE_FORMAT(article_modified_date, \'%d/%m/%Y à %Hh%imin%ss\') AS modified_date_fr, DATE_FORMAT(article_creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS date_fr FROM articles ORDER BY article_creation_date ASC');

        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $articles[] = new Article($data);
        }
        return $articles;
    }

    public function recentArticlesList() {
        $recentArticles = [];
        $q = $this->_db->query('SELECT article_id, article_title, article_content, article_author, article_creation_date FROM articles GROUP BY article_creation_date DESC LIMIT 3');
        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $recentArticles[] = new Article($data);
        }
        return $recentArticles;
    }

    public function setDb(PDO $db) {
        $this->_db = $db;
    }
}