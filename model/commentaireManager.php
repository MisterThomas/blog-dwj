<?php
class CommentaireManager {

    private $_db;

    public function __construct($db) {
        $this->setDb($db);
    }

    public function readCommentaire() {
        $q = $this->_db->query('SELECT commentaire_title, commentaire_content, commentaire_author, commentaire_creation_date FROM commentaires');
        
        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $commentaire = new Commentaire ($data);
        }
        return $commentaire;

        $q->closeCursor();
    }

    public function addCommentaire($commentaire_content, $commentaire_author, $article_id) {
        $q = $this->_db->prepare('INSERT INTO comments (commentaire_content, commentaire_author, article_id, commentaire_creation_date) VALUES (?, ?, ?, NOW())');
        $commentaire = $q->execute(array($commentaire_content, $commentaire_author, $article_id));
        return $commentaire;
    }

    public function deleteCommentaire($article_id, $commentaire_id) {
        $q = $this->_db->prepare('DELETE FROM commentaires WHERE article_id = ? AND commentaire_id = ?');
        $commentaireToDelete = $q->execute(array($article_id, $commentaire_id));
        return $commentaireToDelete;
    }

    public function deleteCommentairesFromArticle($article_id) {
        $q = $this->_db->prepare('DELETE FROM commentaires WHERE article_id = ?');
        $commentaireToDelete = $q->execute(array($article_id));
        return $commentaireToDelete;
    }

    public function listCommentaires() {
        $commentaires = [];
        $q = $this->_db->query('SELECT commentaire_id, commentaire_title, commentaire_content, commentaire_author, DATE_FORMAT(commentaire_creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS commentaire_date_fr FROM commentaires');

        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $commentaires[] = new Commentaire($data);
        }
        return $commentaires;
    }

    public function getCommentairesFromArticle($article_ID) {
        $commentaires = [];
        $q = $this->_db->prepare('SELECT commentaire_id, commentaire_content, commentaire_author, article_id, DATE_FORMAT(commentaire_creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS commentaire_date_fr FROM commentaires WHERE article_id = ? ORDER BY commentaire_creation_date ASC');
        $q->execute(array($article_ID));
        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $commentaires[] = new Commentaire($data);        
        }
        return $commentaires;
    }

    public function reportCommentaire($commentaire_id) {
        $q = $this->_db->prepare('UPDATE commentaires SET commentaire_report_number = commentaire_report_number + 1, commentaire_report_date = NOW() WHERE commentaire_id = ?');
        $reportedCommentaire = $q->execute(array($commentaire_id));
        return $reportedCommentaire;
    }

    public function getReportedCommentaires() {
        $reportedCommentaires = [];getReportedCommentaires
        $q = $this->_db->prepare('SELECT commentaire_id, commentaire_content, commentaire_author, commentaire_creation_date, commentaire_report_number, article_id, DATE_FORMAT(com_report_date, \'%d/%m/%Y à %Hh%imin%ss\') AS commentaire_date_fr FROM commentaires WHERE commentaire_report_number >= 1 ORDER BY commentaire_report_number DESC');
        $q->execute();
        
        while ($data = $q->fetch(PDO::FETCH_ASSOC)) {
            $reportedCommentaires[] = new Commentaire($data);
        } 
        $q->closeCursor();
        return $reportedCommentaires;
    }

    public function setDb(PDO $db) {
        $this->_db = $db;
    }
}