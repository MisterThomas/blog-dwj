<?php
class Comment {
    
    // Décalaration des attributs
    private $_commentaire_id;
    private $_commentaire_content;
    private $_commentaire_author;
    private $_article_id;
    private $_commentaire_creation_date;
    private $_commentaire_date_fr;
    private $_commentaire_report_number;
    private $_commentaire_report_date;

    public function __construct(array $data) {
        $this->hydrate($data);
    }

    // Hydratation
    public function hydrate(array $data) {
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Déclaration des getters (accesseurs)
    public function commentaire_id() {
        return $this->_comment_id;
    }

    public function commentaire_content() {
        return $this->_commentaire_content;
    }

    public function commentaire_author() {
        return $this->_commentaire_author;
    }

    public function article_id() {
        return $this->_article_id;
    }

    public function commentaire_report_date() {
        return $this->_commentaire_report_date;
    }

    public function commentaire_report_number() {
        return $this->_commentaire_report_number;
    }

    public function commentaire_creation_date() {
        return $this->_commentaire_creation_date;
    }

    public function commentaire_date_fr() {
        return $this->_commentaire_date_fr;
    }

    // Déclaration des setters (mutateurs)
    public function setCommentaire_id($commentaire_id) {
        $commentaire_id = (int) $commentaire_id;
        if ($commentaire_id > 0) {
            $this->_commentaire_id = $commentaire_id;
        }
    }

    public function setCommentaire_content($commentaire_content) {
        if (is_string($commentaire_content)) {
            $this->_commentaire_content = $commentaire_content;
        }
    }

    public function setCommentaire_author($commentaire_author) {
        if (is_string($commentaire_author)) {
            $this->_commentaire_author = $commentaire_author;
        }
    }

    public function setArticle_id($article_id) {
        $article_id = (int) $article_id;
        if ($article_id > 0) {
            $this->_article_id = $article_id;
        }
    }

    public function setCommentaire_creation_date($comment_creation_date) {
        $this->_commentaire_creation_date = $comment_creation_date;
    }

    public function setCommentaire_date_fr($comment_date_fr) {
        $this->_commentaire_date_fr = $comment_date_fr;
    }

    public function setCommentaire_report_number($commentaire_report_number) {
        $commentaire_report_number = (int) $commentaire_report_number;
        if ($commentaire_report_number > 0) {
            $this->_commentaire_report_number = $commentaire_report_number;
        }
    } 

    public function setCommentaire_report_date($commentaire_report_date) {
        $this->_commentaire_report_date = $commentaire_report_date;
    }
}