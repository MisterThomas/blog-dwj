class Manager{
	protected function dbConnect(){
		try{
			$bdd=new PDO('mysql:host=localhost;dbname=mini-blog;charset=utf8', 'root','');
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $bdd;
		}
		catch (Exception $e){
			die('Erreur: ' . $e->getmsg());
		}
	}
}