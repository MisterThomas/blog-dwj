<?php
class Article {
    
    // Décalaration des attributs
    private $_article_id;
    private $_article_title;
    private $_article_content;
    private $_article_author;
    private $_article_creation_date;
    private $_date_fr;
    private $_modified_date_fr;
    private $_article_modified_date;

    public function __construct(array $data) {
        $this->hydrate($data);
    }

    // Hydratation
    public function hydrate(array $data) {
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Déclaration des getters (accesseurs)
    public function article_id() {
        return $this->_article_id;
    }

    public function article_title() {
        return $this->_article_title;
    }

    public function article_content() {
        return $this->_article_content;
    }

    public function article_author() {
        return $this->_article_author;
    }

    public function article_creation_date() {
        return $this->_article_creation_date;
    }

    public function date_fr() {
        return $this->_date_fr;
    }

    public function modified_date_fr() {
        return $this->_modified_date_fr;
    }

    public function article_modified_date() {
        return $this->_article_modified_date;
    }

    // Déclaration des setters (mutateurs)
    public function setArticle_id($article_id) {
        $article_id = (int) $article_id;
        if ($article_id > 0) {
            $this->_article_id = $article_id;
        }
    }

    public function setArticle_title($article_title) {
        if (is_string($article_title)) {
            $this->_article_title = $article_title;
        } 
    }

    public function setArticle_content($article_content) {
        if (is_string($article_content)) {
            $this->_article_content = $article_content;
        }
    }

    public function setArticle_author($article_author) {
        if (is_string($article_author)) {
            $this->_article_author = $article_author;
        }
    }

    public function setArticle_creation_date($article_creation_date) {
        $this->_article_creation_date = $article_creation_date;
    }

    public function setDate_fr($date_fr) {
        $this->_date_fr = $date_fr;
    }

    public function setModified_date_fr($modified_date_fr) {
        $this->_modified_date_fr = $modified_date_fr;
    }