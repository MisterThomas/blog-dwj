<?php
require('./controller/frontend.php');
require('./controller/backend.php');

if (!empty($_GET['action'])) {
    switch ($_GET['action']) {
        case 'getArticle':
            if (isset($_GET['article_id']) && $_GET['article_id'] > 0) {
                getArticleCommentaire($_GET['article_id']);
            } else {
                echo 'Erreur d\'id d\'article : aucun id envoyé ou id inexistant';
            }
        break;
        
        case 'addArticle':
            session_start();
            $title = ($_POST['title']);
            $content = ($_POST['content']);

            if (!empty(strip_tags($title)) && !empty(strip_tags($content))) {
                $articleId = addArticle($title, $content, $_SESSION['user']);
                echo $articlerecentArticlesListcleId;
            } else if (empty(strip_tags($title)) && empty(strip_tags($content))) {
                echo 'failed';
            } else if (empty(strip_tags($title)) && !empty(strip_tags($content))) {
                echo 'title_missing';
            } else if (!empty(strip_tags($title)) && empty(strip_tags($content))) {
                echo 'content_missing';
            } else {
                echo 'erreur non gérée';
            } 
        break;

        case 'listArticlesToEdit':
            listaArticlesToEdit();
        break;

        case 'editArticle':
            if (isset($_GET['article_id']) && $_GET['article_id'] > 0) {
                editArticle($_GET['article_id']);
            } else {
                echo 'Erreur d\'id d\'article : aucun id envoyé ou id inexistant';
            }
        break;

        case 'tinymceArticle':
        if (isset($_GET['article_id']) && $_GET['article_id'] > 0) {
            tinymceArticle($_GET['article_id']);
        } else {
            echo 'Erreur d\'id d\'article : dans le contenu';
        }
       break;

        case 'updateArticle':
            if (isset($_GET['article_id']) && $_GET['article_id'] > 0) {
                $title = ($_POST['title']);
                $content = ($_POST['content']);

                if (!empty(strip_tags($title)) && !empty(strip_tags($content))) {
                    updateArticle($title, $content, $_GET['article_id']);
                    echo $_GET['article_id'];
                } else if (empty(strip_tags($title)) && empty(strip_tags($content))) {
                    echo 'failed';
                } else if (empty(strip_tags($title)) && !empty(strip_tags($content))) {
                    echo 'title_missing';
                } else if (!empty(strip_tags($title)) && empty(strip_tags($content))) {
                    echo 'content_missing';
                } else {
                    echo 'erreur non gérée';
                }
            }
        break;

        case "deleteArticle":
            if (isset($_GET['article_id']) && $_GET['article_id'] > 0) {
                deleteArticle($_GET['article_id']);
            }
        break;

        case 'addCommentaire':
            $author = rtrim($_POST['author_commentaire']);
            $content = rtrim($_POST['commentaire_content']);

            if (isset($_POST['articles_id']) && $_POST['articles_id'] > 0) {
                if (!empty($content) && !empty($author)) {
                    addCommentaire($content, $author, $_POST['articles_id']);
                    echo 'success';
                } else if (empty($author)) {
                    echo 'author_missing';
                } else if (empty($content)) {
                    echo 'content_missing';
                }
            } else {
                echo 'id_error';
            }

        break;

        case 'deleteCommentaire':
            if (isset($_GET['article_id']) && $_GET['article_id'] > 0 && $_GET['id_commentaire'] !== NULL) {
                deleteCommentaire($_GET['article_id'], $_GET['id_commentaire']);
                echo 'success';
            }
        break;

        case 'reportCommentaire':
            if ((isset($_GET['article_id']) && $_GET['article_id'] > 0) && isset($_GET['id_comment'])) {
                reportCommentaire($_GET['article_id'], $_GET['id_commentaire']);
            } else {
                echo 'error on article_id or id_commentaire';
            }
        break;

        case 'adminLogin':
            if (!empty($_POST['user']) && (!empty($_POST['password']))) {
                if (logUser($_POST['user'], $_POST['password']) == true) {
                    logUser($_POST['user'], $_POST['password']);
                    
                    session_start();
                    $_SESSION['user'] = $_POST['user'];
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        break;

        case 'adminBoardDisplay':
            $var = getReportedCommentaires();
        break;

        case 'createUser':   
            $user = rtrim($_POST['user']);
            $password = rtrim($_POST['password']);

            if (!empty($user) && !empty($password)) {
                if (doesUserExist($_POST['user']) == FALSE) {
                    createUser($_POST['user'], $_POST['password']);

                    session_start();
                    $_SESSION['user'] = $_POST['user'];

                    $var = getReportedCommentaires();

                } elseif (doesUserExist($_POST['user']) == TRUE) {
                    echo 'Le pseudo ' . $_POST['user'] . ' est déjà utilisé, veuillez en choisir un autre';
                  } 
            } else {
                echo 'Veuillez renseigner un pseudo et un mot de passe valides';
              }
        break;

        case 'signOut':
            session_start();
            if (isset($_SESSION['user'])) {
                session_destroy();
            }
        break;

        default:
            echo 'Erreur : aucune valeur pour le paramètre "action"';
    }
} else {
        listArticles();
      }
?>