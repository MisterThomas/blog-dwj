<section>
	
	<div id="secondSideDeco">
		<aside id="introductionArticle">
			<h3>Les chapitres:</h3>
				<p> Retrouvez ici la liste, complète, des chapitres du nouveau roman de Jean ForteRoche.  </p>
		</aside>
		
		<article id="selectionArticle">

				<?php
					while ($list=$listArticle->fetch() ) {
				?>
				<div class="thumbnail">
					
					<h5><a href="./index.php?action=selectionarticle&amp;id=<?php echo $list['article_id']; ?>"><?php echo htmlspecialchars($list['titre'])?></a>
					</h5>
						<p class="sumArticle"> <?= nl2br($list['article_id']);?> [...]</br></p>
						Mise en ligne le:<?php echo htmlspecialchars($list['date_fr']);?>
				</div>
				<?php
					}
					$listArticle->closeCursor();
				?>
		</article>
	</div>
</section>

</body>
</html>