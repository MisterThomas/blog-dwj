<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta name="description" content="Découvert le nouveau roman de Jean Forteroche">

		<meta name="keywords" content="Billet simple pour l'Alaska le nouveau roman de Jean Forteroche" />
	 
			
		<!--POLICES-->

		<title>Billet simple pour l'Alaska, par JF</title>

	</head>
       <!--tinymce-->
       <script src="assets/tinymce/js/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({ 
				selector:'textarea',
				height:'250px'
			});
		</script>

<body>	
			<header>
				<div id="header_home">

					<h1><a href="./index.php"> "Billet simple pour l'Alaska"</a></h1>
						<div id="banner">
							<div id="presentation"></div>
							<h2> Jean Forteroche</h2>
						</div>
				</div>
					
				<div id="navbar_login">
					<nav>
						<ul>
							<?php
								if (!isset($_SESSION['pseudo'])){
							?> 
								<li>
									<a href="./index.php?action=adminLogin">Connexion</a>
								</li>
							<?php
								}
							?>
								<li>
									<a href="./index.php?action=articles">Les Chapitres</a>
								</li>
							<?php
								if (!isset($_SESSION['id'])|| $_SESSION['id']==115){
									echo "<li><a href='./index.php?action=admin'>Admin</a></li>";
									}
							?> 
								
						</ul>
					</nav>
						<?php
							if (isset($_SESSION['pseudo'] ) ) {
								echo "<p> Bonjour ".$_SESSION['pseudo']."<br/><a href='./index.php?action=signOut'>Déconnexion</a><p>";
							}
						?>
						
				</div>
			</header>

    <!-- Start footer -->
	<footer class="footer">
          <p class="text-center-white">Copyright &copy; Jean Forteroche</p>
    </footer>
    <!-- End footer -->