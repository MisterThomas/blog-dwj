<?php
    if (!isset($_SESSION)) {
        session_start();
    }
?>

<?php $title = 'Blog de Jean Forteroche : Article et commentaires associés'; ?>

<?php ob_start(); ?>
<div class="title">
    <h1>Billet simple pour l'Alaska</h1>
    <?php echo $article->article_title(); ?>
</div>

<div class="commentaireView_article">
    <p>
        <?php echo $article->article_content(); ?>
        Posté le : <?php echo $article->article_date_fr(); ?></br>
        Par : <?php echo $article->article_author(); ?></br>
        Modifié le <?php echo $article->modified_date_fr(); ?>
    </p>
</div>

<div class="comments">
    <p>Commentaires du billet</p>
    <?php foreach ($commentaires as $commentaire) { ?>
    <p>
        Commentaire : <?php echo $commentaire->commentaire_content(); ?></br>
        Posté par : <?php echo $commentaire->commentaire_author(); ?></br>
        le : <?php echo $commentaire->commentaire_date_fr(); ?></br>
        
        <input class="report_commentaire_btn" type="button" id="<?php echo $commentaire->commentaire_id(); ?>" value="Signaler" />
        
    </p>
    <?php } ?>
</div>

<div id="add_commentaire">
    <button>Ajouter un commentaire</button>
</div>

<form id="commentaire_form" action="main_index.php?action=addComentaire" method="post">
    <div>
        <label for="commentaire_author">Pseudo : </label>
        <input class="commentaire_author" type="text" id="author" name="commentaire_author" placeholder="Votre pseudo" required/>
        <span id="Pseudo"></span>
    </div>

    <div>
        <label for="commentaire_content">Votre commentaire : </label>
        <textarea class="commentaire_content" id="content" name="commentaire_content" placeholder="Votre commentaire" required></textarea>
        <span id="Commentaire"></span>
    </div>

    <input type="hidden" id="article_id" name="article_id" value="<?php echo $article->article_id(); ?>" />

    <div>
        <input id="commentaire_submit_btn" type="submit" />
    </div>
</form>

<div id="modal_commentaire" class="modal">
    <div class="modal_content">
        <p id="modal_text"></p>
    </div>
</div>

<script>var artId = <?php echo $article->article_id(); ?>;</script>

<?php $content = ob_get_clean(); ?>
<?php require('view/frontend/inscription.php'); ?>



